# Информация о проекте

jse-07

## Стек технологий

java/Intellij IDEA/Apache Maven/Git

## Требования к SOFTWARE

- JDK 1.8

## Команда для сборки проекта

```bash
mvn clean package
```

## Команда для запуска проекта

```bash
java -jar ./task-manager.jar
```

## Информация о разработчике

**ФИО**: Баулина Ольга Александровна

**E-MAIL**: golovolomkacom@gmail.com
