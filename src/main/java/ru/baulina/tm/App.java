package ru.baulina.tm;

import java.util.Scanner;

public class App {

    public static void main(final String[] args) {
        displayWelcome();
        run(args);
        process();
    }

    private static void process() {
        final Scanner scanner = new Scanner(System.in);
        String command = "";
        while (!TerminalConst.CMD_EXIT.equals(command)) {
            command = scanner.nextLine();
            run(command);
        }
    }

    private static void displayHelp() {
        System.out.println("version - Display program version.");
        System.out.println("about - Display developer info.");
        System.out.println("help - Display list of terminal commands.");
        System.out.println();
    }

    private static void displayVersion() {
        System.out.println("1.0.7");
        System.out.println();
    }

    private static void displayAbout() {
        System.out.println("Olga Baulina");
        System.out.println("golovolomkacom@gmail.com");
        System.out.println();
    }

    private static void exit() {
        System.exit(0);
    }

    private static void displayError() {
        System.out.println("ERROR");
        System.out.println();
    }

    private static void displayWelcome() {
        System.out.println("** WELCOME TO TASK MANAGER **");
    }

    private static void run(final String command) {
        if (command == null || command.isEmpty()) return;
        switch (command.toLowerCase()) {
            case TerminalConst.CMD_HELP:
                displayHelp();
                break;
            case TerminalConst.CMD_VERSION:
                displayVersion();
                break;
            case TerminalConst.CMD_ABOUT:
                displayAbout();
                break;
            case TerminalConst.CMD_EXIT:
                exit();
                break;
            default:
                displayError();
        }
    }

    private static void run(final String[] commands) {
        if (commands == null || commands.length == 0) return;
        final String command = commands[0];
        run(command);
    }

}


